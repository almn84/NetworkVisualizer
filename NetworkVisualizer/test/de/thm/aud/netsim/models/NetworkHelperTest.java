package de.thm.aud.netsim.models;

import de.thm.aud.netsim.models.Network;
import de.thm.aud.netsim.models.NetworkHelper;
import de.thm.aud.netsim.models.Router;
import de.thm.aud.netsim.models.RoutingTable;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.*;
import org.junit.Test;


public class NetworkHelperTest {

    private static final String root = "/home/kristina/Schreibtisch/AuD/NetworkVisualizer/NetworkVisualizer/test/de/thm/aud/netsim/models/";

    @Test
    public void testFromConfigFileOneRouter() throws Exception {
        /**
         * RouterIds:A
         *
         * Connections
         */
        Path p = Paths.get(root + "OneRouter.ncf");

        Network network = NetworkHelper.fromConfigFile(p);

        Router r = network.getRouter("A");
        assertEquals("A", r.getId());

        List<Router> routers = network.routers();

        assertEquals(1, routers.size());
        assertTrue(routers.get(0) == r);

        assertEquals(0, network.neighbors(r).size());
    }

    @Test
    public void testFromConfigFileThreeInLine() throws Exception {
        // A <3> B <4> C

        /**
         * RouterIds:A,B,C
         *
         * Connections
         *   A-B:3
         *   B-C:4
         */
        Path p = Paths.get(root + "ThreeInLine.ncf");

        Network network = NetworkHelper.fromConfigFile(p);

        Router rA = network.getRouter("A");
        assertEquals("A", rA.getId());

        Router rB = network.getRouter("B");
        assertEquals("B", rB.getId());

        Router rC = network.getRouter("C");
        assertEquals("C", rC.getId());

        List<Router> routers = network.routers();
        assertEquals(3, routers.size());
        assertTrue(routers.containsAll(Arrays.asList(rA, rB, rC)));

        List<Router> nOfA = network.neighbors(rA);
        List<Router> nOfB = network.neighbors(rB);
        List<Router> nOfC = network.neighbors(rC);

        assertEquals(1, nOfA.size());
        assertEquals(2, nOfB.size());
        assertEquals(1, nOfC.size());

        assertEquals(rB, nOfA.get(0));
        assertTrue(nOfB.containsAll(Arrays.asList(rA, rC)));

        assertEquals(rB, nOfC.get(0));

        assertEquals(3, network.getCost(rA, rB));
        assertEquals(3, network.getCost(rB, rA));

        assertEquals(4, network.getCost(rB, rC));
        assertEquals(4, network.getCost(rC, rB));
    }

    @Test
    public void testFromConfigFileTriangleRouter() throws Exception {
        /**
         * RouterIds:A,B,C
         *
         * Connections
         *   A-B:3
         *   B-C:4
         *   C-A:9
         */
        Path p = Paths.get(root + "TriangleRouters.ncf");
        Network network = NetworkHelper.fromConfigFile(p);

        Router rA = network.getRouter("A");
        assertEquals("A", rA.getId());

        Router rB = network.getRouter("B");
        assertEquals("B", rB.getId());

        Router rC = network.getRouter("C");
        assertEquals("C", rC.getId());

        List<Router> nOfA = network.neighbors(rA);
        assertEquals(2, nOfA.size());
        assertTrue(nOfA.containsAll(Arrays.asList(rB, rC)));

        List<Router> nOfB = network.neighbors(rB);
        assertEquals(2, nOfB.size());
        assertTrue(nOfB.containsAll(Arrays.asList(rA, rC)));

        List<Router> nOfC = network.neighbors(rC);
        assertEquals(2, nOfC.size());
        assertTrue(nOfC.containsAll(Arrays.asList(rA, rB)));

        assertEquals(3, network.getCost(rA, rB));
        assertEquals(4, network.getCost(rB, rC));
        assertEquals(9, network.getCost(rC, rA));

        assertEquals(3, network.getCost(rB, rA));
        assertEquals(4, network.getCost(rC, rB));
        assertEquals(9, network.getCost(rA, rC));
    }

    @Test
    public void testGenerateRoutingTables() throws Exception {
        /**
         * RouterIds:A,B,C,D,E
         *
         * Connections
         *   A-B:5
         *   A-D:1
         *   B-C:1
         *   B-D:2
         *   D-E:3
         *   E-C:7
         */

        Path p = Paths.get(root + "NetworkConfig.nfc");
        Network network = NetworkHelper.fromConfigFile(p);

        Map<String, RoutingTable> routingTables = NetworkHelper.generateRoutingTables(network);
        RoutingTable rtA = routingTables.get("A");
        RoutingTable rtB = routingTables.get("B");
        RoutingTable rtC = routingTables.get("C");
        RoutingTable rtD = routingTables.get("D");
        RoutingTable rtE = routingTables.get("E");

        assertEquals("A", rtA.getRouterId());

        assertEquals(new Integer(3), rtA.getCost("B"));
        assertEquals(new Integer(4), rtA.getCost("C"));
        assertEquals(new Integer(1), rtA.getCost("D"));
        assertEquals(new Integer(4), rtA.getCost("E"));

        assertEquals("D", rtA.getGateway("B"));
        assertEquals("D", rtA.getGateway("C"));
        assertEquals("D", rtA.getGateway("D"));
        assertEquals("D", rtA.getGateway("E"));


        assertEquals("B", rtB.getRouterId());

        assertEquals(new Integer(3), rtB.getCost("A"));
        assertEquals(new Integer(1), rtB.getCost("C"));
        assertEquals(new Integer(2), rtB.getCost("D"));
        assertEquals(new Integer(5), rtB.getCost("E"));

        assertEquals("D", rtB.getGateway("A"));
        assertEquals("C", rtB.getGateway("C"));
        assertEquals("D", rtB.getGateway("D"));
        assertEquals("D", rtB.getGateway("E"));


        assertEquals("C", rtC.getRouterId());

        assertEquals(new Integer(4), rtC.getCost("A"));
        assertEquals(new Integer(1), rtC.getCost("B"));
        assertEquals(new Integer(3), rtC.getCost("D"));
        assertEquals(new Integer(6), rtC.getCost("E"));

        assertEquals("B", rtC.getGateway("A"));
        assertEquals("B", rtC.getGateway("B"));
        assertEquals("B", rtC.getGateway("D"));
        assertEquals("B", rtC.getGateway("E"));


        assertEquals("D", rtD.getRouterId());

        assertEquals(new Integer(1), rtD.getCost("A"));
        assertEquals(new Integer(2), rtD.getCost("B"));
        assertEquals(new Integer(3), rtD.getCost("C"));
        assertEquals(new Integer(3), rtD.getCost("E"));

        assertEquals("A", rtD.getGateway("A"));
        assertEquals("B", rtD.getGateway("B"));
        assertEquals("B", rtD.getGateway("C"));
        assertEquals("E", rtD.getGateway("E"));


        assertEquals("E", rtE.getRouterId());

        assertEquals(new Integer(4), rtE.getCost("A"));
        assertEquals(new Integer(5), rtE.getCost("B"));
        assertEquals(new Integer(6), rtE.getCost("C"));
        assertEquals(new Integer(3), rtE.getCost("D"));

        assertEquals("D", rtE.getGateway("A"));
        assertEquals("D", rtE.getGateway("B"));
        assertEquals("D", rtE.getGateway("C"));
        assertEquals("D", rtE.getGateway("D"));
    }
}