package de.thm.aud.netsim.models;

import java.io.BufferedReader;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.*;

/**
 * Defines static helper functions for a network.
 *
 * @version 2015-06-10
 * @author Andrej Sajenko, Andrej.Sajenko@mni.thm.de
 */
public final class NetworkHelper {

    public static final int INFINITY = 1000000;

    /**
     * Read a textual network description and create the equivalent Network
     * object. Ließt eine textuelle Netzwerkbeschreibung und erstellt ein
     * entsprechendes Netzwerkobjekt
     *
     * A valid textual network description starts with one line where all
     * RouterIds are declared and separated by a comma (,) Eine gültige
     * Textnetzwerkbeschreibung beginnt mit einer Zeile, in der alle RouterIDs
     * deklariert und durch ein Komma getrennt werden
     *
     * After the router declaration follow the connections. Nach der Router
     * Deklaration folgen die Verbindungen.
     *
     * Each connection belongs to one line and contains the two RouterIds
     * separated by a dash (-) and followed by the connection cost between the
     * two routers. Jede Verbindung gehört zu einer Linie und enthält die beiden
     * RouterIds getrennt durch einen Bindestrich (-) und die Verbindungkosten
     * zwischen den beiden Routern gefolgt
     *
     * The order of the two RouterIds is not relevant. Die Reihenfolge der
     * beiden RouterIds ist nicht relevant.
     *
     * So the connections A-B:5 and B-A:5 are the same, and must not be expected
     * in the file. Also sind die Verbindungen A-B:5 und B-A:5 die gleichen, und
     * müssen in der Datei nicht erwartet werden.
     *
     * Empty lines are optional! Leerzeilen sind optional!
     *
     * Example: RouterIds:A,B,C,D
     *
     * Connections A-B:5 A-D:1 B-C:1 B-D:2
     *
     * @param cfgPath The path to a text file which contains a network
     * description. Der Pfad zu der Textdatei, welche die Netzwerkbeschreibung
     * enthält.
     *
     * @return Network equivalent to the textual network description. Netzwerk
     * entspricht dem textlichen Netzwerkbeschreibung.
     *
     * @throws IOException The textual network description has a different
     * format than the one described. Die textliche Beschreibung Netzwerk ein
     * anderes Format hat als die beschriebenen.
     */
    public static Network fromConfigFile(Path cfgPath) throws IOException {
        Network n = new NetworkImpl();
        BufferedReader br = Files.newBufferedReader(cfgPath, Charset.forName("ISO-8859-1"));

        // startLine
        String startLine = br.readLine();
        if (!startLine.matches("\\b(RouterIds)\\b[:]([A-Z][,])*[A-Z]$")) {
            throw new IllegalArgumentException("StartLine is invalid.");
        }

        // addRouter
        int index = startLine.indexOf(":") + 1;
        for (int i = index; i <= startLine.length(); i += 2) {
            Router router = new Router(Character.toString(startLine.charAt(i)));
            n.addRouter(router);
        }

        // otherLines
        String otherLine = br.readLine();
        while (!otherLine.equals("Connections")) {
            otherLine = br.readLine();
        }

        // addConnection
        String connection = br.readLine();
        while (connection != null) {
            if (!connection.matches(" *?[A-Z][-][A-Z][:][1-9]")) {
                throw new IllegalArgumentException("Connection is invalid.");
            }
            Router r1 = new Router(Character.toString(connection.replaceAll(" ","").charAt(0)));
            Router r2 = new Router(Character.toString(connection.replaceAll(" ","").charAt(2)));
            int cost = Integer.parseInt(Character.toString(connection.replaceAll(" ", "").charAt(4)));
            n.addConnection(r1, r2, cost);
            connection = br.readLine();
        }
        return n;
    }

    /**
     * Take every router from the network and compute a routing table for it.
     * Nehmen jeden Router und Netzwerk, und Berechnen einer Routing-Tabelle für
     * sie.
     *
     * The result is a Map from RouterId and its routing table. Das Ergebnis ist
     * eine Abbildung von routerid und seine Routing-Tabelle.
     *
     * The Map contains all routers of the network. Die Abbildung enthält alle
     * Router des Netzwerks.
     *
     * pre: All Routers are connected pre: Alle Router, die verbunden sind post:
     * The RoutingTables define the shortest path from one to any other router.
     * post: Die RoutingTabellen definieren den kürzesten Path von einer zu der
     * anderen.
     *
     * @param network The connected network Das verbundene Netzwerk
     *
     * @return Map from RouterId and its routing table. EmptyMap if the network
     * contains no routers. Abbildung von routerid und seine Routing-Tabelle.
     * EmptyMap wenn das Netzwerk enthält keine Router.
     */
    public static Map<String, RoutingTable> generateRoutingTables(Network network) {
        Map<String, RoutingTable> routingMap = new HashMap<>();
        network.routers().stream().forEach((routers) -> {
            Map<Router, Integer> distance = new HashMap<>();
            Map<Router, Router> precursor = new HashMap<>();
            Map<Router, Router> gateway = new HashMap<>();
            RoutingTableImpl table = new RoutingTableImpl(routers.getId());
            for (Router r : network.routers()) {
                distance.put(r, INFINITY);
                precursor.put(r, null);
                gateway.put(r, null);
            }
            distance.put(routers, 0);
            for (int i = 0; i < network.routers().size() - 1; i++) {
                network.routers().stream().forEach((u) -> {
                    network.routers().stream().forEach((v) -> {
                        try {
                            if (distance.get(u) + network.getCost(u, v) < distance.get(v)) {
                                distance.put(v, distance.get(u) + network.getCost(u, v));
                                precursor.put(v, u);
                                gateway.put(v, findGateway(network, routers, v, precursor));
                            }
                        } catch (IllegalArgumentException e) {
                        }
                    });
                });
            }
            network.routers().stream().forEach((z) -> {
                try {
                    table.addRouting(z.getId(), gateway.get(z).getId(), distance.get(z));
                } catch (NullPointerException e) {
                }
            });
            routingMap.put(routers.getId(), table);
        });
        return routingMap;
    }

    public static Router findGateway(Network network, Router startRouter, Router endRouter, Map<Router, Router> precursor) {
        if (precursor.get(endRouter).equals(startRouter)) {
            return endRouter;
        } else {
            return findGateway(network, startRouter, precursor.get(endRouter), precursor);
        }
    }
}
