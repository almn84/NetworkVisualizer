package de.thm.aud.netsim.models;

import java.util.*;
import java.util.function.Consumer;
import java.util.function.Function;

public class NetworkImpl implements Network {

    private class Connection {

        Router start, destination;
        int cost;

        private Connection(Router start, Router destination, int cost) {
            this.start = start;
            this.destination = destination;
            this.cost = cost;
        }
    }

    private List<Router> routerList = new ArrayList<>();
    private List<Connection> connection = new ArrayList<>();
    private List<Consumer<Router>> onRouterAddListeners = new ArrayList<>();
    private List<Function<Router, Function<Router, Consumer<Integer>>>> onConnectionAddListeners = new ArrayList<>();

    @Override
    public void registerOnRouterAdd(Consumer<Router> listener) {
        onRouterAddListeners.add(listener);
    }

    @Override
    public void registerOnConnectionAdd(Function<Router, Function<Router, Consumer<Integer>>> listener) {
        onConnectionAddListeners.add(listener);
    }

    @Override
    public void addRouter(Router router) {

        if (router == null) {
            throw new IllegalArgumentException();
        }

        if (routerList.contains(router)) {
            throw new IllegalArgumentException("Router has already been added.");
        }

        if (routerList.contains(router) == false) {
            routerList.add(router);
        }

        for (Consumer<Router> listener : onRouterAddListeners) {
            listener.accept(router);
        }
    }

    @Override
    public void addConnection(Router from, Router to, int cost) {
        
        getRouter(from.getId());
        getRouter(to.getId());

        if (from == null || to == null) {
            throw new IllegalArgumentException();
        }

        /*if (!(routerList.contains(from)) || !(routerList.contains(to))) {
            throw new IllegalArgumentException("Router could not be found.");
        }*/

        connection.add(new Connection(from, to, cost));

        for (Function<Router, Function<Router, Consumer<Integer>>> listener : onConnectionAddListeners) {
            listener.apply(from).apply(to).accept(cost);
        }
    }

    @Override
    public int getCost(Router r1, Router r2) {

        if (r1 == null || r2 == null) {
            throw new IllegalArgumentException();
        }

        if (!(routerList.contains(r1)) || !(routerList.contains(r2))) {
            throw new IllegalArgumentException("Router could not be found.");
        }

        for (Connection c : connection) {
            if (c.destination.equals(r2) && c.start.equals(r1)) {
                return c.cost;
            }
            if (c.start.equals(r2) && c.destination.equals(r1)) {
                return c.cost;
            }
        }

        throw new IllegalArgumentException ("No connection found.");
    }

    @Override
    public List<Router> neighbors(Router router) {
        
        getRouter(router.getId());

        if (router == null) {
            throw new IllegalArgumentException();
        }

        if (!(routerList.contains(router))) {
            throw new IllegalArgumentException("Router was not found!");
        }

        List<Router> neighbor = new LinkedList<>();

        for (Connection c : connection) {
            if (router.equals(c.destination)) {
                neighbor.add(c.start);
            }
            if (router.equals(c.start)) {
                neighbor.add(c.destination);
            }
        }
        return neighbor;
    }

    @Override
    public List<Router> routers() {
        return routerList;
    }

    @Override
    public Router getRouter(String id) {
        for(Router r : routerList) {
            if(r.getId().equals(id)) {
                return r;
            }
        }
        
        throw new IllegalArgumentException("Router could not be found.");
    }
    
    
}
