package de.thm.aud.netsim.models;

/**
 * A network Router with a id.
 *
 * The id represent the Router identity.
 *
 * Router(id = "5").equals(Router(id = "5") == true
 *
 * Immutable
 *
 * @version 2015-06-10
 * @author Andrej Sajenko, Andrej.Sajenko@mni.thm.de
 */
public class Router {
    private String id;

    /**
     * @param id The identity of a router.
     */
    public Router(String id) {
        this.id = id;
    }

    /**
     * @return The identity of a router.
     */
    public String getId() {
        return this.id;
    }

    @Override
    public int hashCode() {
        return id.hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (obj == this) {
            return true;
        }
        return obj.getClass().equals(getClass()) && equals((Router) obj);

    }

    /**
     * Check if this router have the same id as Router r.
     *
     * @param r Router to check.
     *
     * @return true if this router and router r have the same id, else false.
     */
    public boolean equals(Router r) {
        return r.getId().equals(this.id);
    }

    @Override
    public String toString() {
        return "Router {" + id + "}";
    }
}
