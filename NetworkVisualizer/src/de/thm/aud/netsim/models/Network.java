package de.thm.aud.netsim.models;

import java.util.List;
import java.util.function.Consumer;
import java.util.function.Function;

/**
 * Network containing routers and bidirectional connection.
 * Netzwerk mit Router und bidirektionale Verbindung.
 * 
 * Whenever there exists a connection from router A to router B, another connection with the opposite direction (router B to router A) will also be available.
 * Immer, wenn es eine Verbindung von Router zu Router A B wird eine weitere Verbindung mit der entgegengesetzten Richtung (B-Router zu Router A) ebenfalls verfügbar sein.
 *
 * Invariant: If a connection between r1 and r2 exists, the following condition has to be valid:
 * (neighbors(r1) contains r2) AND (neighbors(r2) contains r1) AND getCost(r1, r2) == getCost(r2, r1)
 *
 * @version 2015-06-10
 * @author Andrej Sajenko, Andrej.Sajenko@mni.thm.de
 */
public interface Network {
    /**
     * Add a router to the network.
     *
     * @param router Router to add. If the router already exists, nothing will happen.
     *
     * implSpec: after each successful new added router, all listeners registered with registerOnConnectionAdd must be called.
     */
    void addRouter(Router router);

    /**
     * Add a bidirectional connection from router r1 to router r2 with a connection cost.
     * Nothing will happen, if the connection is already defined.
     *
     * @param r1 Router
     * @param r2 Router
     * @param cost Connection cost
     *
     * implSpec: after each successful new added bidirectional connection all listeners registered with registerOnConnectionAdd must be called.
     *
     * @throws IllegalArgumentException If one of the routers cannot be found in the network.
     */
    void addConnection(Router r1, Router r2, int cost);

    /**
     * @param r1 Router
     * @param r2 Router
     * @return The connection cost between r1 and r2
     *
     * Invariant: getCost(r1, r2) == getCost(r2, r1)
     *
     * @throws IllegalArgumentException If no connection between router r1 and router r2 exists or one of the routers
     * does not belong to the network.
     */
    int getCost(Router r1, Router r2);

    /**
     * An List of all neighbors of the given router
     * @param router The given router
     * @return All neighbors of the router
     *
     * Invariant: for each router R_i element of neighbors(router) the following has to be valid: neighbors(R_i) contains the router as neighbor
     *
     * @throws IllegalArgumentException router does not belong to the network.
     */
    List<Router> neighbors(Router router);

    /**
     * @return An List of all routers in this network.
     */
    List<Router> routers();

    /**
     * @param id Router id
     *
     * @return A Router with the given id
     *
     * @throws IllegalArgumentException If no router could be found with the given router id
     */
    Router getRouter(String id);

    /**
     * @param listener which must be called for each added router
     */
    void registerOnRouterAdd(Consumer<Router> listener);

    /**
     * @param listener which must be called for each added bidirectional connection.
     */
    void registerOnConnectionAdd(Function<Router, Function<Router, Consumer<Integer>>> listener);
}

