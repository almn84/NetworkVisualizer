package de.thm.aud.netsim.models;

import de.thm.aud.netsim.utils.Arrays;
import de.thm.aud.netsim.utils.Condition;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

public class RoutingTableImpl implements RoutingTable {

    /* Debug Test */
    public static void main(String[] args) {
        RoutingTableImpl a = new RoutingTableImpl();
        a.addRouting("router1", "router2", 2);
        a.addRouting("router2", "router8", 7);
        a.addRouting("router5", "router1", 1);
        System.out.println("destinationGatewayMap:  " + a.destinationGatewayMap);
        System.out.println("destinationCostMap:     " + a.destinationCostMap);
        System.out.println("getDestinations():      " + java.util.Arrays.toString(a.getDestinations()));
        System.out.println("getCost(\"router2\"):     " + a.getCost("router2"));
        System.out.println("getGatewaySet():        " + a.getGatewaySet());

    }

    private String routerId; // ist Public, da ein Setter oder Konstruktor fehlt. Ist eins von beiden gewünscht? TODO
    private final Map<String, String> destinationGatewayMap = new HashMap<>();
    private final Map<String, Integer> destinationCostMap = new HashMap<>();

    public RoutingTableImpl(String routerId) {
        this.routerId = routerId;
    }

    public RoutingTableImpl() {
        this.routerId = "";
    }



    @Override
    public String getRouterId() {
        return routerId;
    }

    @Override
    public String[] getDestinations() {
        return destinationGatewayMap.keySet().toArray(
                new String[destinationGatewayMap.keySet().size()]
        );
    }

    @Override
    public Set<String> getGatewaySet() {
        return destinationGatewayMap.values().parallelStream().collect(
                Collectors.toSet()
        );
    }

    @Override
    public String getGateway(String destinationId) {
        return destinationGatewayMap.get(destinationId);
    }


    @Override
    public Integer getCost(String destinationId) {
        return destinationCostMap.get(destinationId);
    }

    @Override
    public void addRouting(String destination, String gateway, Integer cost) {
        destinationGatewayMap.put(destination, gateway);
        destinationCostMap.put(destination, cost);
    }
    // TODO: Hier Arbeiten
}
