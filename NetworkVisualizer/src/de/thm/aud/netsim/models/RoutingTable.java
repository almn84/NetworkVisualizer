package de.thm.aud.netsim.models;

import java.util.Set;

/**
 * A Routing table for one router.
 *
 * The Routing defines the gateway and cost in order to reach a destination,
 * beginning from this routerId.
 *
 * Example:
 * RouterID: A
 * +------------------------------+
 * | Destination | Gateway | Cost |
 * |-------------|---------|------|
 * |     B       |    B    |   3  |
 * |     C       |    B    |   7  |
 * |     D       |    D    |   5  |
 * +------------------------------+
 *
 * @version 2015-06-10
 * @author Andrej Sajenko, Andrej.Sajenko@mni.thm.de
 */
public interface RoutingTable {
    /**
     * @return The Router id which belongs to the calculated routing table.
     */
    String getRouterId();

    /**
     * @return router ids from all destinations.
     */
    String[] getDestinations();

    /**
     * @return A set of all available gateways from all router ids.
     */
    Set<String> getGatewaySet();

    /**
     * @param destinationId The destination router id.
     *
     * @return The gateway router id for this specific destination id.
     */
    String getGateway(String destinationId);

    /**
     * @param destinationId The destination router id.
     *
     * @return The cost, which is necessary in order to reach the destination.
     */
    Integer getCost(String destinationId);

    /**
     * Add a new routing entry
     *
     * @param destination destination router id
     * @param gateway gateway router id
     * @param cost cost to reach the destination
     */
    void addRouting(String destination, String gateway, Integer cost);
}


