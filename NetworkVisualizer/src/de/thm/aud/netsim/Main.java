package de.thm.aud.netsim;

import de.thm.aud.netsim.views.NetworkMenu;
import javafx.application.Application;

/**
 * Application Start!
 *
 * @version 2015-06-10
 * @author Andrej Sajenko, Andrej.Sajenko@mni.thm.de
 */
public class Main {
    public static void main(String[] args) {
        Application.launch(NetworkMenu.class);
    }
}

