package de.thm.aud.netsim.views;

import de.thm.aud.netsim.controllers.Controller;
import de.thm.aud.netsim.controllers.MainController;
import de.thm.aud.netsim.models.Network;
import de.thm.aud.netsim.models.NetworkHelper;
import de.thm.aud.netsim.models.Router;
import de.thm.aud.netsim.utils.Condition;
import de.thm.aud.netsim.utils.FXDialogs;
import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.*;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.stage.Stage;

import java.io.IOException;
import java.nio.file.Path;

public class NetworkMenu extends Application {
    private Controller controller;
    private Network network;
    private NetworkVisualizer networkVisualizer;

    public void bind(Controller controller, Network network) {
        if (controller == null)
            throw new IllegalArgumentException("Controller can not be null");

        this.controller = controller;
        this.network = network;
    }

    private final static String FILE_CHOOSER_FILTER = "*.ncf";
    private final static String FILE_CHOOSER_FILTER_DESC = FILE_CHOOSER_FILTER + " (Network Configuration File)";

    private void closeApplication(Stage stage) {
        networkVisualizer.close();
        stage.close();
    }

    @Override
    public void start(Stage stage) {
        System.out.println("Initialize ...");
        try {
            // Create Model, Controller and View, then Link them together
            final Path configFilePath = FXDialogs.showChooseDialog(stage, FILE_CHOOSER_FILTER, FILE_CHOOSER_FILTER_DESC);

            final Network network = NetworkHelper.fromConfigFile(configFilePath);
            final Controller controller = new MainController(network);
            networkVisualizer = new NetworkVisualizer(network, controller);
            this.bind(controller, network);

            networkVisualizer.visualize();
        } catch (IOException | RuntimeException e) {
            FXDialogs.showException(stage, e);
            System.err.println("Initialization failed!");
            closeApplication(stage);
        }

        stage.setTitle("Network Visualizer - Menu");
        stage.setAlwaysOnTop(true);
        stage.setOnCloseRequest(event ->  closeApplication(stage));


        final Text txtRouterTitle = new Text("Add Router");
        txtRouterTitle.setFont(Font.font("Tahoma", FontWeight.NORMAL, 20));

        final Label lblRouter = new Label("Router Name");
        final TextField tfRouterName = new TextField();
        final TextField tfConnectionCost = new TextField();
        final Button btnAddRouter = new Button("Add");
        btnAddRouter.setOnAction((ActionEvent e) -> {
            try {
                String name = tfRouterName.getText();

                if (!Condition.nullOrEmpty(name)) {
                    controller.addRouter(name);
                } else {
                    FXDialogs.message(stage, "Info", "You have to enter a router name.");
                }
            } catch (IllegalArgumentException ex) {
                FXDialogs.message(stage, "Info", "Router already exist.");
            } catch (Exception other) {
                FXDialogs.showException(stage, other);
                closeApplication(stage);
            }
        });

        final Text txtConnectionTitle = new Text("Add Connection");
        txtConnectionTitle.setFont(Font.font("Tahoma", FontWeight.NORMAL, 20));

        final Label lblRouterA = new Label("Router");
        final Label lblRouterB = new Label("Router");

        ObservableList<String> routerIds = FXCollections.observableArrayList();
        for (Router r : network.routers())
            routerIds.add(r.getId());

        network.registerOnRouterAdd((Router r) -> routerIds.add(r.getId()));

        final ChoiceBox<String> cbRouterA = new ChoiceBox<>(routerIds);
        final ChoiceBox<String> cbRouterB = new ChoiceBox<>(routerIds);


        final Button btnAddConnection = new Button("Add");
        btnAddConnection.setOnAction((ActionEvent e) -> {
            try {
                int cost = Integer.parseInt(tfConnectionCost.getText());
                controller.addConnection(cbRouterA.getValue(), cbRouterB.getValue(), cost);
            } catch (NumberFormatException noNum) {
                FXDialogs.message(stage, "Info", "You have to enter the costs as a decimal number.");
            } catch (IllegalArgumentException illArg) {
                FXDialogs.message(stage, "Info", "You have to select two unconnected routers.");
            } catch (Exception other) {
                FXDialogs.showException(stage, other);
                closeApplication(stage);
            }
        });

        final GridPane grid = new GridPane();
        grid.setAlignment(Pos.CENTER);
        grid.setHgap(10);
        grid.setVgap(10);
        grid.setPadding(new Insets(25, 25, 25, 25));

        final ColumnConstraints cc = new ColumnConstraints();
        cc.setFillWidth(true);
        cc.setHgrow(Priority.ALWAYS);
        grid.getColumnConstraints().addAll(cc, cc);

        grid.add(txtRouterTitle, 0, 0, 2, 1);
        grid.add(lblRouter, 0, 1);
        grid.add(tfRouterName, 1, 1);
        grid.add(btnAddRouter, 2, 1);

        grid.add(txtConnectionTitle, 0, 3, 2, 1);
        grid.add(lblRouterA, 0, 4);
        grid.add(cbRouterA, 1, 4);
        grid.add(lblRouterB, 0, 5);
        grid.add(cbRouterB, 1, 5);
        grid.add(new Label("Cost"), 0, 6);
        grid.add(tfConnectionCost, 1, 6);
        grid.add(btnAddConnection, 2, 6);

        stage.setScene(new Scene(grid, 500, 250));
        System.out.println("... Initialized");

        stage.show();
    }
}
