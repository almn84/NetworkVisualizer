package de.thm.aud.netsim.views;

import de.thm.aud.netsim.controllers.Controller;
import de.thm.aud.netsim.models.NetworkHelper;
import de.thm.aud.netsim.models.Router;
import de.thm.aud.netsim.models.RoutingTable;
import de.thm.aud.netsim.utils.Box;
import org.graphstream.graph.Edge;
import org.graphstream.graph.Graph;
import org.graphstream.graph.Node;
import org.graphstream.graph.implementations.SingleGraph;
import de.thm.aud.netsim.models.Network;

import org.graphstream.ui.view.Viewer;
import org.graphstream.ui.view.ViewerListener;
import org.graphstream.ui.view.ViewerPipe;

import java.util.Map;

/**
 * Visualize a network with router and connections.
 * Can highlight the shortest way to a destination.
 *
 * @version 2015-06-10
 * @author Andrej Sajenko, Andrej.Sajenko@mni.thm.de
 */
public class NetworkVisualizer {

    private final Graph graph;
    private final Network network;
    private final Controller controller;
    private final Box<Boolean> box = new Box<>(true);
    private Map<String, RoutingTable> routingTables;

    public NetworkVisualizer(Network network, Controller controller) {
        this.network = network;
        this.controller = controller;

        graph = new SingleGraph("Network Simulation");
        graph.addAttribute("ui.stylesheet", "node { text-size: 20px; size-mode: fit; shape: rounded-box; fill-color: #FFCC66; stroke-mode: plain; } edge {text-size: 15px; }");

        this.network.registerOnRouterAdd(this::appendRouter);
        this.network.registerOnConnectionAdd((Router a) -> (Router b) -> (Integer cost) ->
                        appendConnection(a, b)
        );

        for (Router from : network.routers()) {
            this.appendRouter(from);
        }
        for (Router from : network.routers()) {
            for (Router to : network.neighbors(from)) {
                this.appendConnection(from, to);
            }
        }
    }

    private void appendRouter(Router r) {
        Node node = graph.addNode(r.getId());
        node.addAttribute("ui.label", r.getId());

        routingTables = null;
    }

    private String connectionId(String a, String b) {
        return (a.compareTo(b) < 0) ? (a + b) : (b + a);
    }

    private String connectionId(Router a, Router b) {
        return connectionId(a.getId(), b.getId());
    }

    private void appendConnection(Router a, Router b) {
        String edgeId = connectionId(a, b);

        if (graph.getEdge(edgeId) == null) {
            Edge edge = graph.addEdge(edgeId, a.getId(), b.getId());
            int cost = network.getCost(a, b);
            edge.addAttribute("length", cost);
            edge.addAttribute("ui.label", cost);

            routingTables = null;
        }
    }

    private void highlightSpanTree(String startId) {
        for (Edge e : graph.getEachEdge()) {
            e.setAttribute("ui.style", "fill-color: black;");
        }
        RoutingTable rt = routingTables.get(startId);

        for (String dest : rt.getDestinations())
            highlightShortWays(rt, dest);
    }

    private void highlightShortWays(RoutingTable rt, String destination) {
        if (rt.getRouterId().equals(destination)) return;

        for (String dest : rt.getDestinations()) {
            if (dest.equals(destination)) {
                String gtw = rt.getGateway(dest);
                graph.getEdge(connectionId(rt.getRouterId(), gtw)).setAttribute("ui.style", "fill-color: red;");
                highlightShortWays(routingTables.get(gtw), destination);
            }
        }
    }

    private ViewerPipe pipe;
    private Viewer viewer;

    public void visualize() {
        System.out.println("Visualizer starting ...");
        viewer = graph.display();
        viewer.setCloseFramePolicy(Viewer.CloseFramePolicy.EXIT);
        pipe = viewer.newViewerPipe();

        pipe.addViewerListener(new ViewerListener() {
            @Override
            public void viewClosed(String s) {
                box.set(false);
                System.out.println("... view closed");
                pipe.pump();
                System.exit(0);
            }

            @Override
            public void buttonPushed(String s) { /* Empty Implementation! */ }

            @Override
            public void buttonReleased(String s) {
                if (routingTables == null) {
                    routingTables = controller.getRoutingTables();
                }
                highlightSpanTree(s);
            }
        });
        final Thread runner = new Thread(() -> {
            System.out.println("... Visualizer started");
            try {
                while (box.get() || !Thread.currentThread().isInterrupted()) {
                    pipe.blockingPump();
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println("... close Visualizer");
        });

        runner.start();

    }

    public void close() {
        viewer.close();
    }
}
