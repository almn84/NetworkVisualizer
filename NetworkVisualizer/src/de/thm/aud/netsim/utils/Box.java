package de.thm.aud.netsim.utils;

/**
 * A box containing exactly one element.
 *
 * This Box can be used to create an additional reference.
 *
 * @param <T> The element type.
 *
 * @version 2015-06-10
 * @author Andrej Sajenko, Andrej.Sajenko@mni.thm.de
 */
public class Box<T> {
    private T elem;

    /**
     * @param elem The element to store.
     *
     * @throws IllegalArgumentException elem == null
     */
    public Box(T elem) { this.set(elem); }

    /**
     * @return The element of the box.
     */
    public T get() { return this.elem; }

    /**
     * Replace the element of the box.
     *
     * @param elem The new element.
     *
     * @throws IllegalArgumentException elem == null
     */
    public void set(T elem) {
        if (elem == null) throw new IllegalArgumentException("Element of the box cannot be null!");
        this.elem = elem;
    }
}
