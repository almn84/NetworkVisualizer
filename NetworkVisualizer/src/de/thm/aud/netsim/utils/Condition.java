package de.thm.aud.netsim.utils;

/**
 * Utility functions to check elements for a specific state.
 *
 * @version 2015-06-10
 * @author Andrej Sajenko, Andrej.Sajenko@mni.thm.de
 */
public class Condition {
    private Condition() {}

    /**
     * Check if a String is null or empty.
     *
     * @param s The string to check
     *
     * @return true if the string is null or empty. Do not check for whitespaces.
     */
    public static boolean nullOrEmpty(String s) {
        return s == null || s.isEmpty();
    }
}
