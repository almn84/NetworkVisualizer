package de.thm.aud.netsim.utils;

import java.lang.reflect.Array;
import java.util.Iterator;

/**
 * Utility functions for Arrays which could not be found in java.util.Arrays
 *
 * @version 2015-06-10
 * @author Andrej Sajenko, Andrej.Sajenko@mni.thm.de
 */
public final class Arrays {
    private Arrays() {}

    /**
     * Create a array from a iterable.
     *
     * @param iterable An iterable
     * @param clazz The Type of the Iterable objects
     * @param <T> The Type of the Iterable objects
     *
     * @return An array of the iterable elements or a empty array.
     */
    public static <T> T[] from(Iterable<T> iterable, Class<T> clazz) {
        int i = 0;
        for (T e : iterable) i++;

        T[] res = (T[]) Array.newInstance(clazz, i);

        int j = 0;
        for (T e : iterable) res[j++] = e;

        return res;
    }
}
