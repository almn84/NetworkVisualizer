package de.thm.aud.netsim.utils;

import de.thm.aud.netsim.utils.Condition;
import javafx.geometry.*;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Priority;
import javafx.scene.text.*;
import javafx.stage.FileChooser;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

import javax.swing.*;
import javax.swing.filechooser.FileFilter;
import javax.swing.plaf.nimbus.State;
import java.awt.*;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.nio.file.Path;

/**
 * Containing some JavaFX based modal dialogs.
 *
 * @version 2015-06-10
 * @author Andrej Sajenko, Andrej.Sajenko@mni.thm.de
 */
public final class FXDialogs {
    private FXDialogs() {}

    /**
     * Show a file chooser dialog.
     *
     * @param root The dialog owner
     * @param fileExtension The file extension (NullAllowed) If null no filter will be applied
     * @param fileDescription The file description (NullAllowed) If null no description will be showed
     * @return The file selected by the dialog
     * @throws IOException If no file was selected
     */
    public static Path showChooseDialog(Stage root, String fileExtension, String fileDescription) throws IOException {
        FileChooser fc = new FileChooser();
        if (!Condition.nullOrEmpty(fileExtension)) {
            FileChooser.ExtensionFilter extFilter = new FileChooser.ExtensionFilter(fileDescription, fileExtension);
            fc.getExtensionFilters().add(extFilter);
        }

        File f = fc.showOpenDialog(root);
        if (f == null) throw new IOException("No File was selected");
        return f.toPath();
    }

    /**
     * Show an exception stack trace in a dialog.
     *
     * pre: ex != null
     *
     * @param owner The dialog owner
     * @param ex The exception to show
     */
    public static void showException(Stage owner, Exception ex) {
        Stage dialog = new Stage(StageStyle.DECORATED);
        dialog.initModality(Modality.WINDOW_MODAL);
        dialog.initOwner(owner);

        dialog.setTitle(ex.getClass().getName());

        BorderPane bl = new BorderPane();

        Scene scene = new Scene(bl, 600, 400);
        dialog.setScene(scene);

        Text title = new Text(ex.getClass().getName());
        BorderPane.setMargin(title, new Insets(10, 10, 10, 10));
        title.setFont(javafx.scene.text.Font.font("Tahoma", FontWeight.NORMAL, 20));

        bl.setTop(title);

        StringWriter sw = new StringWriter();
        PrintWriter pw = new PrintWriter(sw);
        ex.printStackTrace(pw);
        String exceptionText = sw.toString();

        Label label = new Label("The exception stacktrace was:");

        TextArea textArea = new TextArea(exceptionText);
        textArea.setEditable(false);
        textArea.setWrapText(false);

        textArea.setMaxWidth(Double.MAX_VALUE);
        textArea.setMaxHeight(Double.MAX_VALUE);
        GridPane.setVgrow(textArea, Priority.ALWAYS);
        GridPane.setHgrow(textArea, Priority.ALWAYS);

        GridPane expContent = new GridPane();
        expContent.setMaxWidth(Double.MAX_VALUE);

        BorderPane.setMargin(label, new Insets(5, 5, 5, 5));
        BorderPane.setMargin(textArea, new Insets(10, 10, 10, 10));

        expContent.add(label, 0, 0);
        expContent.add(textArea, 0, 1);

        bl.setCenter(expContent);

        dialog.showAndWait();
    }

    /**
     * Show a message dialog.
     *
     * @param owner The dialog owner
     * @param title Title of the dialog
     * @param message The message to display
     */
    public static void message(Stage owner, String title, String message) {
        Stage dialog = new Stage(StageStyle.DECORATED);
        dialog.initModality(Modality.WINDOW_MODAL);
        dialog.initOwner(owner);

        dialog.setTitle(title);

        BorderPane bl = new BorderPane();

        Scene scene = new Scene(bl, 400, 200);
        dialog.setScene(scene);

        Text tTitle = new Text(title);
        BorderPane.setMargin(tTitle, new Insets(10, 10, 10, 10));
        tTitle.setFont(javafx.scene.text.Font.font("Tahoma", FontWeight.NORMAL, 20));

        bl.setTop(tTitle);

        TextArea textArea = new TextArea(message);
        textArea.setEditable(false);
        textArea.setWrapText(true);

        bl.setCenter(textArea);

        bl.setCenter(textArea);

        dialog.showAndWait();
    }

}
