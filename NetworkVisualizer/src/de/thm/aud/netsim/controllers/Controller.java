package de.thm.aud.netsim.controllers;

import de.thm.aud.netsim.models.RoutingTable;

import java.util.Map;

public interface Controller {

    /**
     * @param routerId The Router ID
     *
     * @throws IllegalArgumentException If router already exists.
     */
    void addRouter(String routerId);

    /**
     * Add a connection between the two routers with the given cost.
     *
     * @param routerIdA Router
     * @param routerIdB Router
     * @param cost connection cost
     *
     * @throws IllegalArgumentException If router could not be found by the routerIds or cost is less equals 0
     */
    void addConnection(String routerIdA, String routerIdB, int cost);

    /**
     * Generate Routing tables for the whole network.
     *
     * @return Routing tables.
     */
    Map<String, RoutingTable> getRoutingTables();
}

