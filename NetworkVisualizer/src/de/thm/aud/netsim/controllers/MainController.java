package de.thm.aud.netsim.controllers;


import de.thm.aud.netsim.models.Network;
import de.thm.aud.netsim.models.NetworkHelper;
import de.thm.aud.netsim.models.Router;
import de.thm.aud.netsim.models.RoutingTable;

import java.util.Map;

public class MainController implements Controller {

    private Network network;

    public MainController(Network network) {
        this.network = network;
    }

    @Override
    public void addRouter(String routerId) {
        network.addRouter(new Router(routerId));
    }

    @Override
    public void addConnection(String routerIdA, String routerIdB, int cost) {
        Router a = network.getRouter(routerIdA);
        Router b = network.getRouter(routerIdB);
        network.addConnection(a, b, cost);
    }

    @Override
    public Map<String, RoutingTable> getRoutingTables() {
        return NetworkHelper.generateRoutingTables(network);
    }
}
